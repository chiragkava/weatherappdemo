//
//  APIManager.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//


import Foundation
import UIKit

fileprivate var commonURL = "http://api.openweathermap.org/data/2.5/"
fileprivate var APIKey = "&appid=fae7190d7e6433ec3a45285ffcf55c86"

class APIManager {
    
    typealias HttpResponse = (_ response : Any?,_ error:Error?)->Void
    
    fileprivate struct Static{
        static var instance: APIManager?
    }
    
    class var shared: APIManager{
        if Static.instance == nil{
            Static.instance = APIManager()
        }
        return Static.instance!
    }

    func dispose(){
        APIManager.Static.instance = nil
        print("\n Disposed Singleton instance")
    }
    
    func webApiCall(methodName: String , method:String,parameter: [String: Any] = [:],controller:UIViewController? = nil,isLoader:Bool = false,completionHandler:@escaping HttpResponse)
    {
        if !APIManager.shared.connected() {
            AlertController.alertViewTitle("", message:"No Internr Connection", delegate: controller, cancelButtonTitle: nil, otherButtonTitles: "OK", tag: -1, callBack: nil)
            return
        }
        
        if isLoader == true{
            DispatchQueue.main.async() {
                LoadingView.showLoading()
            }
        }
       
        let finalURL:String = commonURL+methodName+APIKey
        print(finalURL)
        let urlString = finalURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL (string: urlString!)
        var request = URLRequest (url: url!)
        request.httpMethod = method
        request.allHTTPHeaderFields = ["Accept": "application/json", "Content-Type": "application/json"]
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
           DispatchQueue.main.async() {
           if isLoader == true{
                   LoadingView.hideLoading()
           }
           do {
               let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                completionHandler(json,nil)
           } catch {
            completionHandler(nil,error)
           }
           }
        })
        task.resume()

    }
}

extension APIManager {
    
    func connected() -> Bool {
        do{
            let networkReachability = try Reachability()
               if networkReachability.connection != .unavailable {
                   return true
               }
               else {
                   print("There is no internet connection")
                   return false
               }
        }catch
        {
            print(error.localizedDescription)
            return false
        }
    }
}

