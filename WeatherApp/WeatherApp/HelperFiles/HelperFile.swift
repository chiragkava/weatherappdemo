//
//  HelperFile.swift
//  WeatherApp
//
//  Created by chiragkava on 18/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import Foundation
import UIKit

protocol InstantiableFromStoryboard {}
extension InstantiableFromStoryboard {
    static func viewController(storyboard: String = "Main", bundle: Bundle? = nil, identifier: String? = nil) -> Self {
        var strClass = ""
        if let identifier = identifier {
            strClass = identifier
        } else {
            strClass = String(describing: self)
        }
        guard let viewController = UIStoryboard(name: storyboard, bundle: bundle).instantiateViewController(withIdentifier: strClass) as? Self else {
            fatalError("Cannot instantiate view controller of type " + (identifier ?? ""))
        }
        return viewController
    }
}
extension UIViewController: InstantiableFromStoryboard {}

extension UIViewController {

    class func viewControllerMainStoryboard() -> UIViewController {
        return UIViewController.viewController(storyboard:"Main")
    }
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }
}
