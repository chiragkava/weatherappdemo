//
//  LoadingView.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import Foundation
import UIKit
class LoadingView{

    class func showLoading(titleString:String = "Please_WaitDotDotDot") {
        if AppDelegate.shared.indicaterVC == nil {
            AppDelegate.shared.indicaterVC = IndicaterVC.loadFromNib()
            
        }
        if let vc = AppDelegate.shared.indicaterVC{
            if let frame = AppDelegate.shared.window?.bounds{
                vc.view.frame = frame
                AppDelegate.shared.window?.addSubview(vc.view)
                AppDelegate.shared.window?.bringSubviewToFront(vc.view)
            }else{
                AppDelegate.shared.window?.addSubview(vc.view)
                AppDelegate.shared.window?.bringSubviewToFront(vc.view)
            }
        }
    }

    class func hideLoading(completion: (() -> Swift.Void)? = nil) {
        AppDelegate.shared.indicaterVC?.view.removeFromSuperview()
        if let subViews = AppDelegate.shared.window?.subviews{
            for subUIView in subViews {
                if subUIView == AppDelegate.shared.indicaterVC?.view{
                    subUIView.removeFromSuperview()
                }
            }
        }
    }
}
