//
//  AlertController.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import Foundation
import UIKit

class AlertController{
    class func alertViewTitle(_ title: String, message: String, delegate: Any?, cancelButtonTitle btnCancel: String?, otherButtonTitles btnTitles: String, tag: Int, callBack block: ((_ buttonIndex: Int, _ tag: Int) -> Void)?) {
        DispatchQueue.main.async(execute: {() -> Void in

            if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

                if btnCancel != nil {
                    alertController.addAction(UIAlertAction(title: btnCancel, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
                        block?(0, tag)
                    }))
                }

                alertController.addAction(UIAlertAction(title: btnTitles, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    block?(1, tag)
                }))

                if delegate != nil {
                    (delegate as AnyObject).present(alertController, animated: true, completion: nil)
                } else {
                    let vc: UIViewController? = AppDelegate.shared.window?.rootViewController
                    vc?.present(alertController, animated: true, completion: nil)
                }
            } else {
                let alertLogin = UIAlertView(title: title, message: message, delegate: delegate as? UIAlertViewDelegate, cancelButtonTitle: btnCancel, otherButtonTitles: btnTitles)
                if tag >= 0 {
                    alertLogin.tag = tag
                }
                alertLogin.show()
            }
        })
    }
}
