//
//  IndicaterVC.swift
//  WeatherApp
//
//  Created by chiragkava on 18/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit

class IndicaterVC: UIViewController {

    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var horizontalSlider: UISlider!
    @IBOutlet weak var titleLable: UILabel!
    
    let gradient = CAGradientLayer()

    var titleString: String = ""
    var globalTimer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        iconLabel.isHidden = true
        titleLable.textColor = UIColor.white
        titleLable.text = "Please Wait..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startAnimatingWithText(titleText: titleString)
        sliderSetting()
        addGradientWithColor()
    }
        
    func addGradientWithColor() {

        gradient.colors = [UIColor.gray.cgColor,UIColor.gray.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
     func sliderSetting() {
        horizontalSlider.value = 0
        horizontalSlider.minimumValue = 0
        horizontalSlider.maximumValue = 100
        horizontalSlider.isContinuous = true

        DispatchQueue.main.async {
            // timer needs a runloop?
            self.globalTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.updateSlider(timer:)), userInfo: nil, repeats: true)
        }
      
    }

    @objc func updateSlider(timer: Timer) {
        let index = Float(self.horizontalSlider.value + 1)
        self.horizontalSlider.setValue(index, animated: true)
        if self.horizontalSlider.value >= self.horizontalSlider.maximumValue {
            globalTimer?.invalidate()
            sliderSetting()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        globalTimer?.invalidate()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        gradient.frame = self.view.bounds
    }

    func startAnimatingWithText(titleText: String) {
        if titleText == "" {

        } else {
            titleLable.text = "Please Wait..."
        }
    }
}
