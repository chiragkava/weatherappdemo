//
//  LocationListView.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit
import MapKit

class MapView: UIView, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak fileprivate var contentView: UIView!
    let locationManager = CLLocationManager()
    
    var tapCoordinat :(( Double?,Double?)->Void)?
    
    //MARK:- INITIALIZER
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MapView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        randerUI()
    }
    
    func randerUI(){
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(settingTapped))
        tap.delegate = self
        mapView.addGestureRecognizer(tap)
    }
    
    @objc func settingTapped(gestureRecognizer: UITapGestureRecognizer) {
        
        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)

        if tapCoordinat != nil {
            tapCoordinat!(coordinate.latitude, coordinate.longitude)
        }
    }
}

extension MapView: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        let location = CLLocationCoordinate2D(latitude: locValue.latitude,longitude:locValue.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
    
        let annotation = MKPointAnnotation()
        annotation.coordinate = locValue
        mapView.addAnnotation(annotation)
    }
}
