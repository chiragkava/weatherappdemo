//
//  LocationListView.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit

class LocationListView: UIView {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblLocationListView: UITableView!
    @IBOutlet weak fileprivate var contentView: UIView!
    
    var resetArray :((Double?)->Void)?
    var locationDetail :((Double?)->Void)?
    var arryList:[ParseData]{
            set{
                arryOrignal.removeAll()
                arryOrignal.append(contentsOf: newValue)
                filteredData.removeAll()
                filteredData.append(contentsOf: newValue)
            }
            get {
                return filteredData
            }
        }
    var arryOrignal = [ParseData]()
    var filteredData = [ParseData] ()
   
    //MARK:- INITIALIZER
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("LocationListView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        searchBar.delegate = self
        tblLocationListView.register(UINib(nibName: "LocationListCell", bundle: nil), forCellReuseIdentifier: "LocationListCell")
    }
}

extension LocationListView: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        arryList = arryOrignal
        searchBar.becomeFirstResponder()
        
        filteredData = searchText.isEmpty ? arryList : arryList.filter({ (model) -> Bool in
            if let contentName = model.name {
                return (contentName.range(of: searchText, options: .caseInsensitive) != nil)
            }
            return false
        })
        tblLocationListView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        filteredData = arryOrignal
        tblLocationListView.reloadData()
    }
    
}

extension LocationListView: UITableViewDataSource, UITableViewDelegate{
    // MARK: - UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
       let cell = tableView.dequeueReusableCell(withIdentifier: "LocationListCell") as! LocationListCell
       cell.selectionStyle = .none
        cell.name.text = "Name: \(filteredData[indexPath.row].name?.uppercased() ?? "")"
        cell.lat.text =  "Latitude : \(filteredData[indexPath.row].coord?.lat ?? 0)"
        cell.lon.text =  "Longitude: \(filteredData[indexPath.row].coord?.lon ?? 0)"
      
       return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        locationDetail!(filteredData[indexPath.row].addIndex)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
       if (editingStyle == .delete) {
        searchBar.text = ""
        resetArray!(filteredData[indexPath.row].addIndex)
       }
    }
}
