//
//  TransactionDetailsTVCell.swift
//  MobifinElite
//
//  Created by Nilesh on 02/04/20.
//  Copyright © 2020 Panamax IOS Team. All rights reserved.
//

import UIKit

class LanguageTVCell: UITableViewCell {


    @IBOutlet weak var titileLabel: PILable!
    @IBOutlet weak var subTitileLabel: PILable!
    @IBOutlet weak var iconLabel: PILable!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titileLabel.backgroundColor = UIColor.clear
        titileLabel.font = AppFont.SemiBold_Subhead
        titileLabel.textColor = appTheamColor.appPrimaryPink
        
        subTitileLabel.backgroundColor = UIColor.clear
        subTitileLabel.font = AppFont.SemiBold_Subhead
        subTitileLabel.textColor = appTheamColor.appPrimaryPink

        iconLabel.backgroundColor = UIColor.clear
        iconLabel.text = ""
        self.backgroundColor = UIColor.clear

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
