//
//  CustomCollectionCell.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit

class CustomCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblTemprature: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblRainChances: UILabel!
    @IBOutlet weak var lblWindSpeed: UILabel!
    @IBOutlet weak var lblWindEdge: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
