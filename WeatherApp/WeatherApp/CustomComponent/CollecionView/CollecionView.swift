//
//  LocationListView.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit

class CollecionView: UIView{
    
    @IBOutlet weak var collectionList: UICollectionView!
    @IBOutlet weak fileprivate var contentView: UIView!
    
    let reuseIdentifier = "CustomCollectionCell" // also enter this string as the cell identifier in the storyboard
    var arrayItem = [ParseData]()
   
    //MARK:- INITIALIZER
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CollecionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionList.delegate = self
        collectionList.dataSource = self
        collectionList.register(UINib(nibName: "CustomCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CustomCollectionCell")
    }
}

extension CollecionView: UICollectionViewDataSource, UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CustomCollectionCell
         
        cell.lblCityName.text = arrayItem[indexPath.row].city
        cell.lblTemprature.text = "Temperature : " + String(format: "%.0f", arrayItem[indexPath.row].main?.temp ?? 0 as CVarArg)
        cell.lblHumidity.text = "Humidity : " + String(format: "%.0f", arrayItem[indexPath.row].main?.humidity ?? 0 as CVarArg)
        cell.lblRainChances.text = "Rain Chances : " + String(format: "%.0f", arrayItem[indexPath.row].clouds?.all ?? 0 as CVarArg) + "%"
        cell.lblWindSpeed.text = "Wind Speed : " + String(format: "%.0f", arrayItem[indexPath.row].wind?.speed ?? 0 as CVarArg)
        cell.lblWindEdge.text = "Wind Deg : " + String(format: "%.0f", arrayItem[indexPath.row].wind?.deg ?? 0 as CVarArg)
        cell.backgroundColor = UIColor.lightGray
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
}
