//
//  LayoutModel.swift
//  WeatherApp
//
//  Created by chiragkava on 20/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import Foundation

class ParseData : NSObject
{
    var base : String?
    var name : String?
    var visibility: Double?
    var dt:Double?
    var timezone: Double?
    var id:Double?
    var cod:Double?
    var pop:Double?
    var dt_txt:String?
    
    var coord:Coord?
    var weather = [Weather]()
    var main:Main?
    var clouds:Clouds?
    var sys:Sys?
    var wind:Wind?
    var city:String?
    var addIndex:Double?

    init(dict:[String:Any])
    {
        if let value = dict["base"] as? String
        {
            base = value
        }
        if let value = dict["name"] as? String
        {
            name = value
        }
        
        if let value = dict["visibility"] as? Double
        {
            visibility = value
        }
        
        if let value = dict["dt_txt"] as? String
        {
            dt_txt = value
        }
        
        if let value = dict["pop"] as? Double
        {
            pop = value
        }
    
        if let value = dict["dt"] as? Double
        {
            dt = value
        }
        
        if let value = dict["id"] as? Double
        {
            id = value
        }
        
        if let value = dict["timezone"] as? Double
        {
            timezone = value
        }
        if let value = dict["cod"] as? Double
        {
           cod = value
        }
        
        if let dic = dict["coord"] as? [String:Any]
        {
           coord = Coord(dict:dic)
        }
        
        if let dic = dict["main"] as? [String:Any]
        {
           main = Main(dict:dic)
        }
        
        if let dic = dict["clouds"] as? [String:Any]
        {
           clouds = Clouds(dict:dic)
        }
        if let dic = dict["sys"] as? [String:Any]
        {
           sys = Sys(dict:dic)
        }
        if let dic = dict["wind"] as? [String:Any]
        {
           wind = Wind(dict:dic)
        }
        
        if let list = dict["weather"] as? [Any], list.count > 0 {
            for obj in list {
                if let json = obj as? [String:Any] {
                    weather.append(Weather(dict: json))
                }
            }
        }
    }
}

class City : NSObject{
    var cityName : String?
   
    init(dict:[String:Any])
    {
        if let value = dict["name"] as? String
        {
           cityName = value
        }
    }
}

class Coord : NSObject{
    var lat : Double?
    var lon : Double?
    
    init(dict:[String:Any])
    {
        if let value = dict["lat"] as? Double
        {
           lat = value
        }
        if let value = dict["lon"] as? Double
        {
            lon = value
        }
    }
}

class Weather : NSObject{
    
    var id : Int?
    var main : String?
    var disc : String?
    var icon : String?
    
    init(dict:[String:Any])
    {
       if let value = dict["main"] as? String
       {
           main = value
       }
       if let value = dict["description"] as? String
       {
           disc = value
       }
       if let value = dict["icon"] as? String
       {
           icon = value
       }
        if let value = dict["id"] as? Int
        {
                id = value
        }
    }
}

class Main : NSObject{
    
    var temp : Double?
    var feels_like : Double?
    var temp_min : Double?
    var temp_max : Double?
    var pressure : Double?
    var humidity : Double?
    var sea_level : Double?
    var grnd_level : Double?
   
    init(dict:[String:Any])
    {
        if let value = dict["temp"] as? Double
        {
           temp = value
        }
        if let value = dict["feels_like"] as? Double
        {
            feels_like = value
        }
        if let value = dict["temp_min"] as? Double
        {
           temp_min = value
        }
        if let value = dict["temp_max"] as? Double
        {
            temp_max = value
        }
        if let value = dict["pressure"] as? Double
        {
           pressure = value
        }
        if let value = dict["humidity"] as? Double
        {
            humidity = value
        }
        if let value = dict["sea_level"] as? Double
        {
           sea_level = value
        }
        if let value = dict["grnd_level"] as? Double
        {
            grnd_level = value
        }
    }
}

class Wind : NSObject{
    var speed : Double?
    var deg : Double?
    
    init(dict:[String:Any])
    {
       if let value = dict["speed"] as? Double
       {
          speed = value
       }
       if let value = dict["deg"] as? Double
       {
           deg = value
       }
    }
}


class Clouds : NSObject{
    var all : Double?
    init(dict:[String:Any])
    {
       if let value = dict["all"] as? Double
       {
          all = value
       }
    }
}

class Sys : NSObject{
    var sunrise : Double?
    var sunset : Double?
       
       init(dict:[String:Any])
       {
          if let value = dict["sunrise"] as? Double
          {
             sunrise = value
          }
          if let value = dict["sunset"] as? Double
          {
              sunset = value
          }
       }
}
