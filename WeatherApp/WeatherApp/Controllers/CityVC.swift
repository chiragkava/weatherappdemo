//
//  CityVC.swift
//  WeatherApp
//
//  Created by chiragkava on 18/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit

class CityVC: UIViewController {

    var objDetail : ParseData?
    var arryParseData = [ParseData]()
    @IBOutlet weak var stackView: UIStackView!
    
    class func getInstance()-> CityVC {
        return CityVC.viewController(storyboard:"Main")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "City View"
        let strlat = String(format: "%f", objDetail?.coord?.lat ?? 0)
        let strlon = String(format: "%f", objDetail?.coord?.lon ?? 0)
        apiWeatherForcast(methodUrl:"forecast?lat=\(strlat)&lon=\(strlon)")
    }
    
    func renderUI()
    {
        let detailView = LocationDetailView ()
        detailView.backgroundColor = .gray
        detailView.lblCityName.text = objDetail?.name
        detailView.lblTemprature.text = "Temperature : " + String(format: "%.0f", objDetail?.main?.temp ?? 0 as CVarArg)
        detailView.lblHumidity.text = "Humidity : " + String(format: "%.0f", objDetail?.main?.humidity ?? 0 as CVarArg)
        detailView.lblRainChances.text = "Rain Chances : " + String(format: "%.0f", objDetail?.clouds?.all ?? 0 as CVarArg) + "%"
        detailView.lblWindSpeed.text = "Wind Speed : " + String(format: "%.0f", objDetail?.wind?.speed ?? 0 as CVarArg)
        detailView.lblWindEdge.text = "Wind Deg : " + String(format: "%.0f", objDetail?.wind?.deg ?? 0 as CVarArg)
        self.stackView.addArrangedSubview(detailView)
        
        let collecionView = CollecionView ()
        collecionView.backgroundColor = .gray
        collecionView.arrayItem = arryParseData
        self.stackView.addArrangedSubview(collecionView)
    }
    
    func apiWeatherForcast(methodUrl:String)
    {
        APIManager.shared.webApiCall(methodName:methodUrl, method: "GET", isLoader: true){ (response,error)  in
            LoadingView.hideLoading()
            if error == nil {
                if let dict = response as? [String: Any] {
                    if let list = dict["list"] as? [Any], list.count > 0 {
                        for obj in list {
                            if let json = obj as? [String:Any] {
                                let data = ParseData(dict: json)
                                if let cityDict = dict["city"] as? [String : Any]
                                {
                                    if let name = cityDict["name"] as? String
                                    {
                                        data.city = name
                                    }
                                }
                                self.arryParseData.append(data)
                            }
                        }
                        self.renderUI()
                        print("Count :\(self.arryParseData.count)")
                    }
                }
            }
            else {
                AlertController.alertViewTitle("", message: error?.localizedDescription ?? "Something Went Wrong", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK", tag: -1, callBack: nil)
            }
        }
    }
}
