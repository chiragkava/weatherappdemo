//
//  SettingVC.swift
//  WeatherApp
//
//  Created by chiragkava on 18/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit
import WebKit

class HelpVC: UIViewController, WKUIDelegate, WKNavigationDelegate {

     @IBOutlet weak var webViewContainer: UIView!
    var webView: WKWebView!
    
    class func getInstance()-> HelpVC {
        return HelpVC.viewController(storyboard:"Main")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "City View"
        
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.webViewContainer.frame.size.width, height: self.webViewContainer.frame.size.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.webViewContainer.addSubview(webView)
        webView.topAnchor.constraint(equalTo: webViewContainer.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: webViewContainer.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: webViewContainer.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: webViewContainer.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: webViewContainer.heightAnchor).isActive = true
        webView.uiDelegate = self
        webView.navigationDelegate = self
        loadUserGuide("Userguide")
    }
    
    func loadUserGuide(_ htmlPageName : String){
        if let url = Bundle.main.url(forResource: "\(htmlPageName)", withExtension:"html"){
            let request = NSURLRequest(url: url)
            webView.load(request as URLRequest)
        }
    }
}

