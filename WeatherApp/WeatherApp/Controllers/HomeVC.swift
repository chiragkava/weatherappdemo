//
//  HomeVC.swift
//  WeatherApp
//
//  Created by chiragkava on 18/09/20.
//  Copyright © 2020 chiragkava. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    var arryParseData = [ParseData]()
    
    let mapView = MapView()
    let listView = LocationListView()
    var index:Double = 0.0
    
    class func getInstance()-> HomeVC {
        return HomeVC.viewController(storyboard:"Main")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationControllerUI()
        self.title = "Home View"
        renderUI()
       
    }
    
    func setNavigationControllerUI()
    {
        self.navigationController?.navigationBar.isHidden = false
        let setting = UIBarButtonItem(title: "Help", style: .plain, target: self, action: #selector(settingTapped))
        self.navigationItem.rightBarButtonItem = setting
    }
    
    func renderUI(){
        
        listView.backgroundColor = .gray
        listView.resetArray = { [weak self]( removeIndex) in
            let filter = self?.arryParseData.filter{$0.addIndex != removeIndex}
            self?.listView.arryList.removeAll()
            self?.listView.arryList = filter ?? []
            self?.arryParseData = filter ?? []
            self?.listView.tblLocationListView.reloadData()
        }
        listView.locationDetail = { [weak self]( removeIndex) in
            let filter = self?.arryParseData.filter{$0.addIndex == removeIndex}
            let vc = CityVC.getInstance()
            vc.objDetail = filter?.first
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        stackView.addArrangedSubview(listView)
        
        mapView.backgroundColor = .gray
        mapView.tapCoordinat = { [weak self](lat, lon) in
            
            let strlat = String(format: "%f", lat ?? 0)
            let strlon = String(format: "%f", lat ?? 0)
            self?.apiCallGetBookmarkedList(methodUrl:"weather?lat=\(strlat)&lon=\(strlon)")
        }
        stackView.addArrangedSubview(mapView)
    }
    
    @objc func settingTapped()
    {
        let helpVC = HelpVC.getInstance()
        self.navigationController?.pushViewController(helpVC, animated: true)
    }
    
    func apiCallGetBookmarkedList(methodUrl:String)
    {
        APIManager.shared.webApiCall(methodName:methodUrl, method: "GET", isLoader: true){ (response,error)  in
            LoadingView.hideLoading()
            if error == nil {
                if let dict = response as? [String: Any] {
                    let data = ParseData(dict: dict)
                    self.index = self.index + 1.0
                    data.addIndex = self.index
                    self.arryParseData.append(data)
                    self.listView.arryList = self.arryParseData
                    self.listView.tblLocationListView.reloadData()
                }
            }
            else {
                AlertController.alertViewTitle("", message: error?.localizedDescription ?? "Something Went Wrong", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK", tag: -1, callBack: nil)
            }
        }
    }
}
